'''
Maya's script editor has a fixed set of keybindings; however, if we're using
Maya2011 or later, we can install our own by creating an event filter on 
script editor widgets

Requires PyQt
'''
__version__ = 0.1
__author__ = "John Patrick"

import logging, inspect
from PyQt4 import QtCore, QtGui
import sip
import maya.OpenMayaUI as omUi
import maya.mel as mel

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


def toQtObject(mayaName):
    '''
    @author: Nathan Horne
    
    Get the QObject of a UI Control'''
    p = omUi.MQtUtil.findControl(mayaName)

    if p is None:
        p = omUi.MQtUtil.findLayout(mayaName)
        if p is None:
            p = omUi.MQtUtil.findMenuItem(mayaName)
    if p is not None:
        return sip.wrapinstance(long(p), QtCore.QObject)


class SEActions(QtCore.QObject):
    '''
    Manage a list of actions that can be assigned shortcuts
    
    Actions are added to the top-level menubar of the script
    editor, and given a priority of 129 (one higher than the
    default actions)
    '''
    __single = None
    def __new__(cls, *args, **kwargs):
        if type(cls) != type(cls.__single):
            cls.__single = super(SEActions, cls).__new__(cls, *args, **kwargs)
        return cls.__single
    
    def __init__(self, parent=None):
        super(SEActions, self).__init__(parent=parent)
        self._parent = parent
        self.editorWindow = toQtObject('scriptEditorPanel1Window')
        self.menuBar = self.editorWindow.children()[1].children()[1].children()[1]
        
        #make custom menu
        self.menu = None
        for child in self.menuBar.children():            
            try:
                if child.title() == 'Custom Actions':
                    self.menu = child
                    self.menu.clear()
                    break
            except:
                pass
        if self.menu is None:
            self.menu = QtGui.QMenu('Custom Actions', self.menuBar)
            self.menuBar.addMenu(self.menu)
            
        #the lists of QActions
        self._standardActions = {} #builtin
        self._customActions= {} #ours
        
        
        #find all 'do_' methods and make actions
        methods = inspect.getmembers(self, inspect.ismethod)
        for methodStr, method in methods:
            if methodStr.startswith('do_'):
                methodStr = " ".join(methodStr.split('_')[1:])
                
                actn = QtGui.QAction(methodStr, self)
                self.connect(actn, QtCore.SIGNAL('triggered()'), method)
                self._customActions[methodStr] = actn
                if methodStr == 'print active widget info':
                    shortcut = QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_O)
                    actn.setShortcut(shortcut)
                if methodStr == 'backward char':
                    shortcut = QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_Z)
                    actn.setShortcut(shortcut)
                actn.setPriority(QtGui.QAction.HighPriority)
                self.menu.addAction(actn)
        #make a menu for custom actions
        
        #get all actions in the menus
        actns = _getActions(self.menuBar)
        for a in actns.keys():
            self._standardActions[str(a.text())] = a
    
    #TODO:  Finish
    def _textEditor(self, num=None):
        '''
        Return the QObject of the specified editor, or the currently active editor
        if one not specified.
        
        If the number specified is out of range, the last editor is returned.
        
        Returns a tuple of (editorObj, tabNumber) of the actual object returned
        '''
        
        if num:
            num = int(num)            
            objName = "cmdScrollFieldExecuter%i" % num
            try:
                obj = toQtObject(objName)
                return (obj, num)
            except:
                raise NotImplementedError
        else:
            ctl = mel.eval('getCurrentExecuterControl()')
            num = int(ctl[-1])
            obj = toQtObject(ctl)
            return (ctl, num)    
                       
    def listActions(self):
        '''
        Return a dic of all actions defined in the menu as {actionText: QActionObj}
        '''
        pass

    def triggerAction(self, action):
        '''
        Trigger the action, if it exist
        '''
        qaction = self._customActions.get(action, None)
        if qaction:
            try:
                qaction.trigger()
                
            except Exception, e:
                _logger.warnign('Could not trigger action "%s":\n%s'\
                                 % (action, str(e)))                
        return self
    
    #Custom Action
    def do_print_active_widget_info(self):
        fw = QtGui.QApplication.focusWidget()        
        print fw
        #print "Children: %s" % fw.children()
        
    def do_set_mark(self):
        self._moveMode=0
        
    def do_quit_command(self):
        self._moveMode=1
           
    def do_forward_char(self):
        c = self._editor.textCursor()
        c.movePosition(QtGui.QTextCursor.NextCharacter, mode=self._moveMode)
        self._editor.setTextCursor(c)
        return False
    
    def do_backward_char(self):
        c = self._editor.textCursor()
        c.movePosition(QtGui.QTextCursor.PreviousCharacter, mode=self._moveMode)
        self._editor.setTextCursor(c)
        return False
    
    def do_previous_line(self):
        c = self._editor.textCursor()
        #c.movePosition(QtGui.QTextCursor.P, mode=self._moveMode)
        self._editor.setTextCursor(c)
        return False
    
    def do_next_line(self):
        pass
    
    def do_forward_word(self):
        c = self._editor.textCursor()
        c.movePosition(QtGui.QTextCursor.NextWord, mode=self._moveMode)
        self._editor.setTextCursor(c)
        return False
    
    def do_backward_word(self):
        c = self._editor.textCursor()
        c.movePosition(QtGui.QTextCursor.PreviousWord, mode=self._moveMode)
        self._editor.setTextCursor(c)
        return False
    
    def do_beginning_of_line(self):
        pass
    
    def do_end_of_line(self):
        pass
    
    def do_beginning_of_text(self):
        pass
    
    def do_end_of_text(self):
        pass

class KeyMap(QtCore.QObject):
    keymap = {frozenset(['C', 'f']): 'forward_char',
              frozenset(['C', 'b']): 'backward_char'}
    

g_scriptEditorShortcuts = None 
def thisObj():
    return toQtObject(mel.eval('getCurrentExecuterControl()'))
    
def _getActions(menu):
    '''
    Recurse through submenus and get all QActions.
    Return dict of {action: shortcutList}
    '''
    result = {}
    for actn in menu.actions():
        shortcuts = actn.shortcuts()
        if shortcuts:
            result[actn] = shortcuts
        else:
            result[actn] = []
    submenus = [m for m in menu.children() if isinstance(m, QtGui.QMenu)]
    for menu in submenus:
        result.update(_getActions(menu))
    return result


#def disableScriptEditorShortcuts():
#    '''
#    Remove all shortcuts from QActions in Script editor
#    menu.  Return a dict that can be used to restore them
#    '''
#    editorWindow = toQtObject('scriptEditorPanel1Window')
#    menuBar = editorWindow.children()[1].children()[1].children()[1]
#    shortcuts = _getShortcuts(menuBar)
#    for actn in shortcuts.keys():
#        actn.setShortcuts([])
#                
#    #keep these in a place where they won't be garbage collected
#    global g_scriptEditorShortcuts
#    if g_scriptEditorShortcuts is None:
#        g_scriptEditorShortcuts = shortcuts
#        
#    return shortcuts
#
#def restoreScriptEditorShortcuts(shortcutDct=None):
#    if not shortcutDct:
#        shortcutDct = g_scriptEditorShortcuts
#    if shortcutDct is None:
#        raise RuntimeError("No shortcuts to restore")
#    for actn, shortcutList in shortcutDct.items():
#        actn.setShortcuts(shortcutList)