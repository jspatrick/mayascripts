import re
import maya.cmds as MC

def getEntsOfType(types):
    result = []
    for e in MC.ls(type='spEntityNode'):
        if MC.spEntityQuery(e, t=1) in types:
            result.append(e)
    return result
    
def getBadModelPaths(entity):
    cloneName = MC.spEntityQuery(entity, cl=1)
    
    mts = MC.spEntityQuery(entity, mts=1)
    
    badModelPaths = {}
    for mt in mts:        
        dagPath = MC.spEntityQuery(entity, mt=mt, dagPath=1)
        pathParts = dagPath.split('|')
        for part in pathParts:
            if not re.match('%s((_.*)|$)' % cloneName, part):
                badModelPaths[mt] = dagPath
                break
            
    return badModelPaths

    
def fixBadModelPaths(entity):    
    clone = MC.spEntityQuery(entity, cl=1)
    modelTags = getBadModelPaths(entity)
    for mt, path in modelTags.items():
        parts = path.split('|')
        newParts = []
        for part in parts:
            if '_' in part:
                newParts.append(re.sub("^[a-zA-Z0-9]+_", '%s_' % clone, part))
            else:
                newParts.append(clone)
        newPath = '|'.join(newParts)
        translate = MC.spEntityQuery(entity, mt=mt, translate=1)
        if not MC.objExists(newPath) and translate == 'yes':            
            raise RuntimeError("new path %s from %s does not exist" % (newPath, path))
        
        else:
            MC.spEntityEdit(entity, mt=mt, dagPath=newPath)
            print "Updated %s to %s" % (mt, newPath)
        

def getBadAsmbProps():
    ents = getEntsOfType(['procedural_prop_asmb'])
    result = []
    for e in ents:
        if getBadModelPaths(e):
            result.append(e)
    return result

