from pymel.core import * #@UnusedWildImport
## orient the joint with an up-locator

def jpOrientJoint(joint, loc, aimVector=[1,0,0], upVector=[0,1,0]):
	#if either joint or loc not provided, query from selection
	joint = PyNode(joint)
	loc = PyNode(loc)
	childJnt = listRelatives(joint, children=True)
	parent(childJnt, world=True)

	#zero orients
	joint.jointOrientX.set(0)
	joint.jointOrientY.set(0)
	joint.jointOrientZ.set(0)

	#aim at next jnt using up loc
	aimCst = aimConstraint (childJnt, joint, offset=[0,0,0], aimVector= aimVector, upVector=upVector, worldUpType = "object",
		worldUpObject = loc)

	delete(aimCst)
	#get rotations
	rots = joint.rotate.get()
	#zero rotations
	joint.rotate.set([0,0,0])
	#set orients
	joint.jointOrientX.set(rots[0])
	joint.jointOrientY.set(rots[1])
	joint.jointOrientZ.set(rots[2])
	#reparent child joint
	parent (childJnt, joint)

def jpOrientSel(aimVector=[1,0,0], upVector=[0,1,0]):
	'orient the joints with jpOrientTJoint based on selection'
	loc, joint = ls(sl=True)
	jpOrientJoint(joint, loc, aimVector, upVector)


