from __future__ import with_statement
import sys, math, re, tempfile, logging
#import pymel.core as PM
import maya.OpenMaya as OM
import maya.api.OpenMaya as OM2
import maya.cmds as MC
import maya.mel as MM
import cPickle as pickle

_logger = logging.getLogger(__name__)
#try studio specific imports
try:
    from jpSpiTools import *
except ImportError:
    _logger.warning("Skipping studio-specific tools")
    
def getAutoTangentCurves(char):
    crvs = MC.ls('%s_*' % char, type=['animCurveTA', 'animCurveTU', 'animCurveTL']) or []

    result = set()
    
    for crv in crvs:
        inTang = MC.keyTangent(crv, q=1, inTangentType=1)
        outTang = MC.keyTangent(crv, q=1, outTangentType=1)
        if 'auto' in inTang or 'auto' in outTang:
            result.add(crv)
            
    return list(result)


# def getAllMatrices(node):
#     '''Return a dict of all matrices'''
#     node = PM.PyNode(node)
#     result = {}
#     for a in node.listAttr(settable=1):
#         if a.isArray():
#             "Getting array for %s" % a.name()
#             for i in range(a.numElements()):
#                 try:
#                     m = a.elementByLogicalIndex(i).get()
#                     if isinstance(m, PM.dt.Matrix):
#                         result['%s[%i]' % (a.name(), i)] = m
#                     else:
#                         print "matrix"
#                 except:
#                     break
#         try:
#             m = a.get()
#         except:
#             pass
#         else:
#             if isinstance(m, PM.dt.Matrix) and '[' not in a.name():
#                 result[a.name()] = m
#     return result


# def printMatches(matrixDict1, matrixDict2, printDiffs=False, skipIdentity=False):
#     identityM = PM.dt.Matrix()
#     for ctlMName, ctlM in matrixDict1.items():
#         for smMName, smM in matrixDict2.items():
#             identity=""
#             if smM == identityM:
#                 identity="(IDENTIY!)"
#             if ctlM == smM and not printDiffs:
#                 if identity and skipIdentity:
#                     pass
#                 else:
#                     print "match%s: %s == %s" % (identity, ctlMName, smMName)
#             elif ctlM != smM and printDiffs:
#                 print "diff%s: %s != %s" % (identity, ctlMName, smMName)


def diffMatrices(matrixDict1, matrixDict2):
    for key, matrix in matrixDict1.items():
        node, attr = key.split('.')
        try:
            key2 = [a for a in matrixDict2.keys() if a.split('.')[1] == attr][0]
        except IndexError:
            print ("No matrix for attr '%s' in matrixDict2" % attr)
            continue
        matrix2 = matrixDict2[key2]
        if matrix != matrix2:
            for i in range(4):
                for j in range(4):
                    if abs(matrix[i][j] - matrix2[i][j]) > .001:
                        print ("%s != %s" % (key, key2))
                
def getWeights(deformer, targetGeo=""):
    '''get tuple of (index, wt)'''
    indices = MC.deformer(deformer, q=1, geometryIndices=1)
    geo = MC.deformer(deformer, q=1, geometry=1)
    geoIndexMap = {}
    for i in range(len(geo)):
        geoIndexMap[geo[i]] = indices[i]
    if not targetGeo:
        targetGeo = geoIndexMap.keys()[0]
    indices = MC.getAttr('%s.weightList[%i].weights' % (deformer, geoIndexMap[targetGeo]), multiIndices=1)
    vals = MC.getAttr('%s.weightList[%i].weights' % (deformer, geoIndexMap[targetGeo]))[0]
    result = {}
    for i in range(len(indices)):
        result[indices[i]] = vals[i]
    return result


def applyWeights(vertWtMap, deformer, targetGeo=""):
    indices = MC.deformer(deformer, q=1, geometryIndices=1)
    geo = MC.deformer(deformer, q=1, geometry=1)
    geoIndexMap = {}
    for i in range(len(geo)):
        geoIndexMap[geo[i]] = indices[i]
    if not targetGeo:
        targetGeo = geoIndexMap.keys()[0]
        
    geoIndex = geoIndexMap[targetGeo]
    for index, wt in vertWtMap.items():
        MC.setAttr('%s.weightList[%i].weights[%i]' %(deformer, geoIndex, index), wt)


#useful for getting python data between maya sessions
TEMPFILE = os.path.join(tempfile.tempdir, 'jpTmpFile.pkl')
def dumpTmpData(data):
    """Dump to a temp file"""
    with open(TEMPFILE, 'w') as f:
        pickle.dump(data, f)

def loadTmpData():
    with open(TEMPFILE, 'r') as f:
        f.seek(0)
        return pickle.load(f)



def bakeSubframes(node, startFrame, endFrame, inc=.25):
    '''bake transformation anim on subframes between whole frames'''
    startFrame = int(startFrame)
    endFrame = int(endFrame)
    frames = range(startFrame, endFrame)
    frames.append(endFrame)
    for i in range(len(frames)-1):
        thisFrame = frames[i]
        nextFrame = frames[i]
        subFrame = thisFrame + inc
        startLoc = MC.spaceLocator()[0]
        endLoc = MC.spaceLocator()[0]
        
        #create locators at each frame
        for loc, frame in [(startLoc, startFrame), (endLoc, endFrame)]:
            MC.currentTime(frame, update=1)
            MC.parent(loc, node)
            _setIdentity(loc)
            MC.parent(loc, w=1)        

        while subFrame < nextFrame:
            subFrame += inc
    
def strToObj(node, asHandle=False):
    """
    Return an MObject from a string node
    @param node: the node name
    @type node: str
    @param asHandle: Return an MObjectHandle
    @type asHandle: True
    @return: the api object
    @rtype: MObject or MObjectHandle
    """
    sl = OM.MSelectionList()
    sl.add(node)
    obj = OM.MObject()
    sl.getDependNode(0, obj)
    if asHandle:
        obj = OM.MObjectHandle(obj)

    return obj


def strToDagPath(node):
    """
    Return an MDagPath instance form a string node
    @param node: the node name
    @type node: str
    @return: MDagPath instace
    @rtype: MDagPath
    """
    sl = OM.MSelectionList()
    sl.add(node)
    dp = OM.MDagPath()
    sl.getDagPath(0, dp)
    return dp


def nodeToStr(node):
    """Get a string node name from an api object
    @param node: the node object
    @type node: MObject, MObjectHandle, or MDagPath
    @return: node name
    @rtype: str
    """
    if isinstance(node, OM.MDagPath):
        return node.partialPathName()

    if isinstance(node, OM.MObjectHandle):
        if not node.isValid:
            raise RuntimeError("object no longer exists")
        node = node.object()

    if not isinstance(node, OM.MObject):
        raise RuntimeError("Invalid arg - %r" % node)

    if node.hasFn(OM.MFn.kDagNode):
        pathArray = OM.MDagPathArray()

        OM.MDagPath.getAllPathsTo(node, pathArray)
        if pathArray.length() > 1:
            _logger.warning("Multiple paths for node, returning first")

        return pathArray[0].partialPathName()

    return OM.MFnDependencyNode(node).name()

    
def getShape(node):
    """If node is a shape node, return it.  If it is
    a transform with a single shape parent, return the shape.

    @param node: the shape or transform node
    @raise RuntimeError: if node is not a shape and has multiple
      or no shape children
    """

    if MC.objectType(node, isAType='geometryShape'):
        return node

    result = None
    shapes = MC.listRelatives(node) or []
    for shape in shapes:
        #ni flag broken?
        if MC.getAttr('%s.intermediateObject' % shape):
            continue

        if MC.objectType(shape, isAType='geometryShape'):
            if result is not None:
                raise RuntimeError("Multiple shapes under '%s'" % node)
            result = shape

    if result is None:
        raise RuntimeError("No shapes under '%s'" % node)
    
    return result
            
def closestPointOnNurbsObj(xformNode, nurbsObj, worldSpace=False):
    """Get closest point to a nurbs surface or curve in the curve's
    object space

    @param xformNode: a transform node
    @type xformNode: str
    @param nurbsObj: the curve xform or shape node
    @type nurbsObj: str
    @param: return the point in worldspace
    @return: closest world-space position on the nurbs object
    @rtype: 3-float list, ie [3.4, 2.3, 4.4]
    """

    shape = getShape(nurbsObj)
    shapeXform = MC.listRelatives(shape, parent=1)[0]
    dp = strToDagPath(shape)
    node = dp.node()
    if node.hasFn(OM.MFn.kNurbsCurve):
        fn = OM.MFnNurbsCurve(dp)
    elif node.hasFn(OM.MFn.kNurbsSurface):
        fn = OM.MFnNurbsSurface(dp)
    else:
        raise RuntimeError("Invalid node type for %s" % shape)
    crvParentMatrix = strToDagPath(shapeXform).inclusiveMatrix()

    #get the position in objet space of the curve
    nodePos = OM.MPoint(*MC.xform(xformNode, q=1, rp=1, ws=1))
    if worldSpace:
        nodePos *= crvParentMatrix.inverse()

    pnt = fn.closestPoint(nodePos)

    if worldSpace:
        #back to world space
        pnt *= crvParentMatrix

    return [pnt.x, pnt.y, pnt.z]

    
def closestParamOnSurface(node, surf):
    surf = getShape(surf)

    pnt = closestPointOnNurbsObj(node, surf)
    srfFn = OM.MFnNurbsSurface(strToDagPath(surf))
    suU = OM.MScriptUtil()
    suV = OM.MScriptUtil()
    pDoubleU = suU.asDoublePtr()
    pDoubleV = suV.asDoublePtr()

    srfFn.getParamAtPoint(OM.MPoint(*pnt), pDoubleU, pDoubleV)

    return (suU.getDouble(pDoubleU), suV.getDouble(pDoubleV))



def bindToFollicle(obj, surf, par=False):
    """
    Create a follice at the closest point to the object
    """
    surf = getShape(surf)
    folName = obj + '_fol' 


    param = closestParamOnSurface(obj, surf)    
    #follicles are weird.  They create a transform that's not returned
    fol = MC.createNode('follicle')
    xform = MC.listRelatives(fol, parent=1)
    xform = MC.rename(xform, obj + '_fol')
    fol = MC.listRelatives(xform)[0]

    MC.setAttr("%s.pu" % fol,  param[0])
    MC.setAttr("%s.pv" % fol, param[1])
    
    MC.connectAttr('%s.worldMatrix[0]' % surf, '%s.inputWorldMatrix' % fol)
    MC.connectAttr('%s.local' % surf, '%s.inputSurface' % fol)

    MC.connectAttr('%s.outRotate' % fol, '%s.r' % xform)
    MC.connectAttr('%s.outTranslate' % fol, '%s.t' % xform)
    if par:
        MC.parent(obj, xform)
    else:
        MC.parentConstraint(xform, obj, mo=True)
    return (fol, xform)
