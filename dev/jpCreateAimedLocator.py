"""
Create oriented locator
"""
import maya.cmds as MC
import maya.OpenMaya as OM

def _fixName(loc):
    if not loc.startswith('|'):
        loc = '|' + loc
    return loc
    
def doIt():
    
    sel = MC.ls(sl=1)
    numItems = len(sel)
    assert (numItems == 2 or numItems ==3), "select 2 or 3 items"

    auxLocs = []
    for i in range(numItems):
        l = MC.spaceLocator(name='aux_loc_%i' % i)[0]
        MC.delete(MC.pointConstraint(sel[i], l))
        auxLocs.append(_fixName(l))
        
    xform = MC.createNode('transform', name='locator_aim_xform')
    MC.delete(MC.pointConstraint(auxLocs[0], xform, mo=False))
    
    aimKwargs = {'aimVector': [1,0,0],
                 'upVector': [0,1,0]}
    if numItems == 3:
        aimKwargs['worldUpObject'] = auxLocs[2]
        aimKwargs['worldUpType'] = 'object'
    else:
        aimKwargs['worldUpType'] = 'scene'

    MC.delete(MC.aimConstraint(auxLocs[1], xform, **aimKwargs))

    loc = _fixName(MC.spaceLocator(name='aimed_loc')[0])
    for attr in ['X', 'Y', 'Z']:
        MC.setAttr('%s.localScale%s' % (loc, attr), 50)
    MC.parent(loc, xform)
    loc = xform + loc
    MC.makeIdentity(loc)
    
    pos1 = MC.xform(auxLocs[0], q=1, t=1, ws=1)
    pos2 = MC.xform(auxLocs[1], q=1, t=1, ws=1)
    p1 = OM.MVector(pos1[0], pos1[1], pos1[2])
    p2 = OM.MVector(pos2[0], pos2[1], pos2[2])
    
    length = (p1 - p2).length()

    MC.setAttr('%s.tx' % loc, length/2.0)
    MC.delete(auxLocs)
    
def makeBone(dist = 50.0):
    sel = MC.ls(sl=1)
    assert len(sel) == 1, "Select one node"
    sel = sel[0]
    
    positions = []
    names = ['base', 'tip']
    for name in names:        
        node = _fixName(MC.createNode('transform', name=name))
        MC.parent(node, sel)        
        node = sel + node
        MC.makeIdentity(node)
        if name == 'tip':
            MC.setAttr('%s.tx' % node, dist)
        positions.append(MC.xform(node, q=1, t=1, ws=1))

        MC.delete(node)
        
    MC.select(cl=1)
    jnts = []
    for i, pos in enumerate(positions):
        jnts.append(MC.joint(p=pos, n=names[i]))
        
    for jnt in jnts:
        MC.joint(jnt, e=1, zso=1, oj='yzx', sao='zup')
        
    MC.select(sel)
