Author: John Patrick
jp@canyourigit.com
2/1/2010
Description:
The goal of this script was to simplify the creation and modification of parent and orient constraints for
animators and riggers.  The concept behind this script is that the user defines certain objects as "children",
and then can define other objects within the scene as parents of the child.  The script sets up the 
constraints, and creates an attribute on the "child" object that makes it simple to switch the parent 
without having to dig for the proper constraints and weights.  It also creates the constraints on offset
nodes it places above the "child" object, so that the animator is free to animate the node beneath the
constraints (and without having to really even think about them!)

Installation:
This script relies on PyMEL 1.0 or greater, which must be installed in order for it to work.
Visit http://code.google.com/p/pymel/downloads/list to download, and read "install.html" in
the docs folder for PyMEL installation instructions.

Once PyMEL is installed, place gsSpaceSwitching in your Maya Scripts directory.

Execute the following lines from a Python command-line in Maya:
    import gsSpaceSwitching
    reload(gsSpaceSwitching)

Workflow/Use:
See the video link at http://www.canyourigit.com/tools.php under GS Space Switching for use.