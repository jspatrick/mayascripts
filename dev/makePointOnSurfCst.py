import maya.cmds as MC
import maya.OpenMaya as OM

def strToNode(nodeStr, asObjHandle=False):
    """
    Get an MObject instance from a string.
    @param getHandle: return an MObjectHandle instead of the MObject
    """
    sl = OM.MSelectionList()
    sl.add(nodeStr)
    obj = OM.MObject()
    sl.getDependNode(0, obj)

    if asObjHandle:
        result = OM.MObjectHandle(obj)
    else:
        result = obj
    return result

def strToDagPath(nodeStr):
    """
    Get an MDagPath instance from a string
    """
    sl = OM.MSelectionList()
    sl.add(nodeStr)
    dag = OM.MDagPath()
    try:
        sl.getDagPath(0, dag)
    except RuntimeError:
        logger.warning("%s is not a Dag node. Returning None" % nodeStr)
        dag = None
    return dag

def nodeToStr(node):
    """
    Get a string version of a node from node.  By default, the shortest
    unique name is returned for dag nodes

    @param node:  the OpenMaya object instance
    @type node: MObject, MObjectHandle, or MDagPath
    """
    #first, get an mObject from any input
    if isinstance(node, OM.MDagPath):
        if not node.isValid():
            _logger.warning("MDagPath has invalid node; returning None")
            return None
        else:
            mObject = node.node()
    elif isinstance(node, OM.MObjectHandle):
        if not node.isValid():
            _logger.warning("MObjectHandle has invalid node; returning None")
            return None
        else:
            mObject = node.object()
    elif isinstance(node, OM.MObject):
        mObject = node
    else:
        raise RuntimeError("Invalid node type: %s" % str(type(node)))

    #is the mObject a dag node?
    if mObject.hasFn(OM.MFn.kDagNode):
        #get the dag path
        dp = OM.MDagPath.getAPathTo(mObject)
        return dp.partialPathName()

    else:
        #get the DG node name
        mfnDN = OM.MFnDependencyNode(mObject)
        return mfnDN.name()


def __closestUVPoint(geoFn, wsPoint):
    
    su = OM.MScriptUtil()
    p_f2 = su.asFloat2Ptr()
    geoFn.getUVAtPoint(basePoint, p_f2, OM.MSpace.kWorld)
    u= su.getFloat2ArrayItem(p_f2, 0, 0)
    v= su.getFloat2ArrayItem(p_f2, 0, 1)
    return (u, v)

def closestUVPoint(geo, wsPoint):
    cpom = MC.createNode('closestPointOnMesh')
    MC.connectAttr('%s.outMesh' % geo, "%s.inMesh" % cpom)
    MC.connectAttr('%s.worldMatrix[0]' % geo, "%s.inputMatrix" % cpom)
    MC.setAttr('%s.worldMatrix[0]' % geo, "%s.inputMatrix" % cpom)
    
def follicleNearNode(refNode, geo, name='follicle'):
    
    if not MC.objectType(geo, isAType='mesh'):
        geo = MC.listRelatives(geo, type='mesh')[0]
    
    geoFn = OM.MFnMesh(strToDagPath(geo))
    
    refPoint = OM.MPoint(*MC.xform(refNode, t=1, ws=1, q=1))
    (u, v) = _closestUVPoint(geoFn, refPoint)
    
    folShape = MC.createNode('follicle', name=name)
    folXform = MC.listRelatives(folShape, parent=1)[0]

    MC.connectAttr('%s.worldMatrix[0]' % geo, '%s.inputWorldMatrix' % folShape)
    MC.connectAttr('%s.outMesh' % geo, '%s.inputMesh' % folShape)
    MC.connectAttr('%s.outTranslate' % folShape, '%s.translate' % folXform)
    MC.connectAttr('%s.outRotate' % folShape, '%s.rotate' % folXform)

    MC.setAttr('%s.parameterU' % folShape, u)
    MC.setAttr('%s.parameterV' % folShape, v)
    
    return folShape


def makePOSNode(baseRefNode, aimRefNode, upRefNode, geo):
    """
    """
    baseFol = follicleNearNode(baseRefNode, geo)
    aimFol = follicleNearNode(aimRefNode, geo)
    upFol = follicleNearNode(upRefNode, geo)    

    
    
    
    
    
