import re, sys, os

def stripEclipseColorPrefs(file):
    """Take an exported preferences file from Eclipse and strip it down to the
    color preferences"""
    x = re.compile(r""" ^/instance/        #first thing will always /instance
                        (
                        org.eclipse.ui.editors
                        |
                        org.python.pydev/.*?=(\d{3},?){3}  #ends with 111,222,333
                        )
                        .*?$                #optionally other chars then EOL
                    """, re.VERBOSE)
    newFile = ""
    try:
        print file
        #open file for reading
        f = open(file, 'r')
        for line in f.readlines():
            if x.match(line):
                newFile+=line
    except IOError:
        print "File Error!"
    finally:
        f.close()
    return newFile

def saveNewFile(file):
    """Save a new file"""
    newFile = "_COLORS".join(os.path.splitext(file))
    try:
        f = open(newFile, "w")
        f.write(stripEclipseColorPrefs(file))
    except IOError, e:
        print e
        
    finally:
        f.close()
    

if __name__ == "__main__":
    saveNewFile(sys.argv[1])