#!/usr/bin/python
import re
import maya.OpenMaya as OM
import maya.cmds as MC
from string import ascii_lowercase
from PyQt4 import QtGui, QtCore

def nameToNode(name):
    sl = OM.MSelectionList()
    node =OM.MObject()
    sl.add(name)
    sl.getDependNode(0, node)
    return node


def nodeToName(node):
    if node.hasFn(OM.MFn.kDagNode):
        dp = OM.MDagPath.getAPathTo(node)
        return dp.partialPathName()

    dnFn = OM.MFnDependencyNode(node)
    return dnFn.name()

    
def _newBase(number, base):
    """
    Return a sequence of numbers for the new base, ie 16 (hex), 2(binary), etc
    """
    
    number = int(number)
    base = int(base)
    seq = []
    
    if number == 0:
        return [0]
    
    while number > 0:
        seq.insert(0, number % base)
        number = number / base
    
    return seq
    
def alphaToken(count, upper=False):
    #convert to base-26 number, use as indices for alphabet
    seq = [x-1 for x in _newBase(count-1, 26)]
    seq[-1] += 1
    seq = [ascii_lowercase[i] for i in seq]
    result =  ''.join(seq)
    if upper:
        return result.upper()
    return result


SPECIAL_CHARACTERS = ['#', '@', r'\$', '%']


def tokenize(string):
    toks = []
    start_pos = 0
    end_pos = 0
    for i, char in enumerate(string):
        if char in SPECIAL_CHARACTERS:
            if end_pos > start_pos:
                toks.append(string[start_pos:end_pos])
            toks.append(char)
            start_pos = end_pos = i+1
        else:
            end_pos = i + 1
    if end_pos > start_pos:
        toks.append(string[start_pos:end_pos])
    return toks


def format_name(pattern, object_num, orig_name):
    toks = tokenize(pattern)
    result = []
    for tok in toks:
        if tok == '#':
            result.append(str(object_num))
        elif tok == '@':
            result.append(alphaToken(object_num))
        elif tok == '$':
            result.append(alphaToken(object_num, upper=True))
        elif tok == '%':
            result.append(orig_name)
        else:
            result.append(tok)
    return ''.join(result)


def hashRename(namePattern, nodes=None):
    if not nodes:
        nodes = MC.ls(sl=1) or []
    nodes = [nameToNode(x) for x in nodes]
    
    result = []
    for i, node in enumerate(nodes):        
        name = nodeToName(node)
        newName = format_name(namePattern, i+1, name)
        print "Renaming %s --> %s" % (name, newName)
        result.append(nameToNode(MC.rename(name, newName)))
        
    return [nodeToName(x) for x in result]
    

class HashRenameDialog(QtGui.QDialog):
    def __init__(self, initialText=None, parent=None):
        super(HashRenameDialog, self).__init__(parent=parent)
        layout = QtGui.QVBoxLayout(self)
        msg = "Rename with special tokens"
        msg += "\n\t'#': number"
        msg += "\n\t'@': lowercase-letter"
        msg += "\n\t'$': uppercase-letter"        
        msg += "\n\t'%': original node name"
        layout.addWidget(QtGui.QLabel(msg))
        self.le = QtGui.QLineEdit()
        if initialText:
            self.le.setText(initialText)
        layout.addWidget(self.le)

        bbox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok| \
                                      QtGui.QDialogButtonBox.Cancel)
        layout.addWidget(bbox)
        bbox.accepted.connect(self.accept)
        bbox.rejected.connect(self.reject)

    def accept(self):
        hashRename(str(self.le.text()))
        return super(HashRenameDialog, self).accept()
        

def replaceInNames(orig, new, nodes=None):
    if not nodes:
        nodes = MC.ls(sl=1) or []        
    nodes = [nameToNode(x) for x in nodes]
    r = []
    for node in nodes:
        nodeName = nodeToName(node)
        sn = nodeName.split('|')[-1]        
        sn = re.sub(orig, new, sn)
        r.append(nameToNode(MC.rename(nodeName, sn)))
        
    return [nodeToName(x) for x in r]

    
class ReplaceInNameDialog(QtGui.QDialog):
    def __init__(self,  parent=None):
        super(ReplaceInNameDialog, self).__init__(parent=parent)
        layout = QtGui.QVBoxLayout(self)
        msg = "Replace in node name"
        layout.addWidget(QtGui.QLabel(msg))
        self.orig = QtGui.QLineEdit()
        self.orig.setProperty('placeholderText', 'orig')
        self.new = QtGui.QLineEdit()        
        self.new.setProperty('placeholderText', 'replace with')
        
        layout.addWidget(self.orig)
        layout.addWidget(self.new)
        
        bbox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok| \
                                      QtGui.QDialogButtonBox.Cancel)
        layout.addWidget(bbox)
        bbox.accepted.connect(self.accept)
        bbox.rejected.connect(self.reject)
        

        self.move(QtGui.QCursor().pos())


    def accept(self):
        replaceInNames(str(self.orig.text()), str(self.new.text()))
        return super(ReplaceInNameDialog, self).accept()


def initUI():
    sel = MC.ls(sl=1)
    if sel:
        cur = sel[0].split('|')[0]
    else:
        cur = ''
        
    ui_inst = HashRenameDialog(cur)
    ui_inst.move(QtGui.QCursor().pos())
    ui_inst.exec_()
    
if __name__ == '__main__':
    print _newBase(16, 2)
    print alphaToken(27)
    
