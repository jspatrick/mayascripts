import maya.mel as MM
import maya.cmds as MC
import os
import sys


def correctSysPath():
  MAYA_SEP = '/'
  for i, path in enumerate(sys.path):
    if path[-1] in (MAYA_SEP, os.sep, os.altsep):
      sys.path[i] = sys.path[i][:-1]
correctSysPath()


def etom_pyexec(fname):
  f = open(fname)
  try:
    c = f.read()
    try:
      return str(eval(c))
    except:
      exec c in locals(), globals()
  finally:
    f.close()

def __appendPaths__():
    appDir = os.environ['MAYA_APP_DIR']
    scripts = os.path.join(appDir, 'scripts')
    dirs = [os.path.join(scripts, d) for d in os.listdir(scripts)]
    dirs = filter(os.path.isdir, dirs)
    print dirs
    msPath = os.environ['MAYA_SCRIPT_PATH'].split(os.pathsep)
    for d in dirs:
        msPath.append(d)
        sys.path.append(d)
    os.environ['MAYA_SCRIPT_PATH'] = ':'.join(msPath)
        
    MM.eval('rehash')

def colorizeMaya():
    import spColorizeMaya
    spColorizeMaya.colorize()
    
__appendPaths__()
