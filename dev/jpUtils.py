import maya.cmds as MC

def connectOneToMany(outputObjAttr, inputObjs, inputAttrs):
    """
    This utility connects a specified object's attribute to any number of input
    object attributes.
    """
    if type(outputObjAttr) != type(""):
        print ("Must specify a single output object attribute")
        return
    if type(inputObjs) != type([]) or type(inputAttrs) != type([]):
        print ("input Obj(s) and input attr(s) must be list of strings")
        return
    for obj in inputObjs:
        for attr in inputAttrs:
            inputAttr = ("%s.%s" % (obj, attr))
            try:
                MC.connectAttr(outputObjAttr, inputAttr, f=True)
            except:
                raise Exception, "can't connect " + outputObjAttr + " to " + inputAttr

def distDict(testObj, targetList):
    '''
    Returns a dictionary of {target1: distance to testObj, ...}
    '''
    import cmath
    distDict = {}
    testObjPos = MC.xform(testObj, q=True,a=True, ws=True, t=True)
    for target in targetList:
        tgtPos = MC.xform(target, q=True,a=True, ws=True, t=True)
        vector = [tgtPos[0]-testObjPos[0], tgtPos[1]-testObjPos[1],tgtPos[2]-testObjPos[2]]
        mag = abs(cmath.sqrt((vector[0]**2) + (vector[1]**2) + (vector[2]**2)))
        distDict[target] = mag
    return distDict

def distDict2(testObj, targetList):
    '''
    Return the names of the two closest target objects and their distances
    from the targets
    '''
    #get the distance dictionary
    d = distDict(testObj, targetList)
    #l is a temporary list that can be sorted to get the lowest 2 values.
    l = d.values()
    l.sort()
    l = [l[0], l[1]]
    resultKeys = [k for k,v in d.items() if v in l]
    #create a dictionary with the keys and values from the dict
    return {resultKeys[0]: d[resultKeys[0]], resultKeys[1]: d[resultKeys[1]]}

def testDist(testObj, targetList):  
    '''
    Return the name of the closest target object
    '''
    d = distDict(testObj, targetList)
    minD = min([v for k,v in d.items()])
    return [k for k, v in d.items() if v==minD][0]


def jpDistWeights(testDict):
    '''
    Convert a dictiony of [{"object": distance}, {"object2":distance2}]
    to [{"object": distanceWeight}, {"object": distanceWeight}]
    '''
    result = {}
    totalDist = sum(testDict.values())
