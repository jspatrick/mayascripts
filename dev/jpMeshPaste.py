"""'Pastes' a mesh onto another mesh.

To use, execute the following in a python editor:
import gsMeshPaste
gsMeshPaste.initUI()
"""

__version__ = "1.0"
import pymel.core as pm

def pasteMesh(*args):
    "Select an object, and paste all other selected objects to it"
    sel = pm.ls(sl = True)
    mesh = sel[0]
    bills = sel[1:]

    cps = pm.createNode("nearestPointOnMesh")
    shape = mesh.getShape()
    shape.outMesh >> cps.inMesh
    
    for bill in bills:
        
        for vtx in bill.vtx:
            p = vtx.getPosition(space="world")
            cps.inPosition.set(p)
            vtx.setPosition(cps.position.get(), space="world")
            cps.inPosition
    pm.delete(cps)


class UI(object):
    def __init__(self):	
        self.name = "meshPaseWin"
        self.title = "Mesh-on-Character Paster"
    
        if pm.window(self.name, exists = True):
            pm.deleteUI(self.name)
        if pm.windowPref(self.name, exists=True):
            pm.windowPref(self.name, remove=True)
    
        #create window
        self.win = pm.window(self.name, title = self.title + " -v" + __version__, w=150,h=100)
        #layout#
        layout= pm.verticalLayout(numberOfDivisions=100)
        pm.text("Select character,\nthen select meshes")
        pm.button(label="Paste Meshes!", c=pm.Callback(pasteMesh))
        layout.redistribute()
        self.win.show()
        
def initUI():
    g_ui = UI()
