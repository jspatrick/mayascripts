def renameSelJnts():
    from string import ascii_lowercase as AL
    import pymel.core as PM
    jnts = PM.ls(sl=1)
    i=0
    j=0

    for jnt in jnts:
        name = "policetapewrap_ct_cn_%s%s" % (AL[j], AL[i])    
        i += 1
        if i > 25:
            i = 0
            j += 1
        jnt.rename(name)



