import logging
import maya.cmds as MC
import maya.mel as MM
import maya.OpenMaya as OM


_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)

def pointMatrixMult(point, matrix):
    """
    Return the new point
    """
    pass

def surfaceOnNodes(nodes, name='jntsSrf', upAxis=0):    
    inPos = [0,0,0]
    outPos = [0,0,0]
    inPos[upAxis] = -1
    outPos[upAxis] = 1
    
    cmd = ['surface -du 2 -dv 1 -name "%s" ' % name]

    cmd.append('-ku 0')
    for i in range(len(nodes)-1):
        cmd.append('-ku %i' % i)
    cmd.append('-ku %i' % (len(nodes)-2))
    
    cmd.append('-kv 0 -kv 1')
    
    for node in nodes:
        wsp = MC.xform(node, q=1, rp=1, ws=1)
        cmd.append("-p %f %f %f" % (wsp[0] + inPos[0],
                                    wsp[1] + inPos[1],
                                    wsp[2] + inPos[2]))

        cmd.append("-p %f %f %f" % (wsp[0] + outPos[0],
                                    wsp[1] + outPos[1],
                                    wsp[2] + outPos[2]))

    cmd = " ".join(cmd)
    _logger.debug(cmd)
    
    return MM.eval(cmd)
    
if __name__  == "__main__":
    jnts = MC.ls(sl=1)
    print MC.xform(jnts[0], q=1, m=1, ws=1)
    
