"""
This module implements a dynamic space-switching UI that allows the user
to create and delete dynamic parent, point, and orient constraints between
objects in a scene.  It also implements a UI to allow the user to seamlessly 
switch between different 'parent' objects without the object jumping.

Installation:
This script relies on PyMEL 1.0 or greater, which must be installed in order for it to work.
Visit http://code.google.com/p/pymel/downloads/list to download, and read "install.html" in
the docs folder for PyMEL installation instructions.
"""
__version__ = "2.0.0"
__author__ = "John Patrick"
__email__ = "jspatrick@gmail.com"
__date__ = "12/10/2011"

import pymel.core as pm

###########################################################################
##---Error Classes
###########################################################################

class SSError(Exception):
    pass
class SSBadInputError(SSError):
    pass
class SSInterfaceError(SSError):
    pass

###########################################################################
##---Space-Switching Classes
###########################################################################

class Parent (object):
    """Data that defines a 'parent' object and methods that
    allow the user to create, modify, and delete these objects"""
    def __init__(self, parent):
        """Determine whether parent is already a 'dynamic' parent, and
        set up the instance appropriately."""
        self.node = pm.PyNode(parent)
        self.children = {}
        #set up the parent as an SS parent if not already
        if not hasattr(self.node, "ss_driverGrp"):
            #create the driver group
            self.driverGrp = pm.group(world=True, empty=True, name = self.node.name()+"_ssDriver")
            #snap the empty group to the parent
            snap(self.node, self.driverGrp)

            #make it a child of the parent
            pm.parent(self.driverGrp, self.node)

            #add custom attributes to the parent objects and the parent driver groups
            self.node.addAttr("ss_driverGrp", at="message")
            self.driverGrp.addAttr("ss_driverObj", at="message")
            self.node.message >> self.driverGrp.ss_driverObj
            self.driverGrp.message >> self.node.ss_driverGrp
        else:
            self.driverGrp = self.node.ss_driverGrp.listConnections()[0]

    def removeGroups(self):
        """Delete the offset nodes and constraints that allow this object
        to be a parent"""
        self.node.ss_driverGrp.delete()
        pm.delete(self.driverGrp)

    #def getChildren(self):
        #"""Return a dictionary of {childNode: ChildObject}"""
        #children = self.sn.listConnections()
CHILD_ATTR = 'ss_child_tag'
class Child (object):
    """Data that defines a 'child' object and methods that
    allow the user to create, modify, and delete these objects"""
    def __init__(self, child, ssType=None):
        """Determine whether child is already a 'spaceSwitching' child, 
	and set up the instance appropriately.  'ssType' is the type of child, 
	and can be parent(default), point, orient, or hybrid"""

        self.node = pm.nt.Transform(child)
        self.setupGroups(ssType)            
            
    def setupGroups(self, ssType):
        self.hybridGrp = None
        if not self.node.hasAttr(CHILD_ATTR):
            self.node.addAttr(CHILD_ATTR, dt='string')
            #if no type provided, set to parent
            if not ssType:
                ssType = "parent"
            #add an attribute to identify the type of constraint
            self.node.addAttr("ss_type", dt="string")
            self.node.ss_type.set(ssType.lower())
            #create the driven group node above the child
            self.drivenGrp = pm.group(world=True, empty=True, name = "%s_ssParentDriven" % self.node.name())
            #snap the drivenGrp to the node
            snap(self.node, self.drivenGrp)
            #get the child object's parent - this returns empty if it's world
            childParent= self.node.getParent()
            #make the child and dynParent group siblings if it's parent isn't world
            if childParent:
                pm.parent(self.drivenGrp, childParent)
            #parent the child object under the new DynParentDriven group
            pm.parent(self.node, self.drivenGrp)
            #add an enum attr
            self.node.addAttr("parentTo", at="enum", en="None", keyable = True)
            #add some message attributes to the child
            self.node.addAttr("ss_drivenGrp", at="message")

            self.drivenGrp.message >> self.node.ss_drivenGrp
            #add some message attributes to the childGrp
            self.drivenGrp.addAttr("ss_drivenObj", at="message")
            self.node.message >> self.drivenGrp.ss_drivenObj
            #add a new attr to the scriptNode

        #else, our node has already been 'set up'
        else:
            #if no type provided, get it from the current node
            if ssType is None:
                ssType = self.node.ss_type.get()
            #if ssType was provided and it's not what the node currently is, raise an exception
            if ssType != self.node.ss_type.get():
                raise SSError("Can't reclassify a child object as a new type if it has already been setup.")
            self.drivenGrp = self.node.ss_drivenGrp.listConnections()[0]

        #if this is a hybrid child, we need to add and extra group or make sure
        #one already exists.  Also need to add a second enum for the orient
        if ssType.lower() == "hybrid":
            try:
                hybridAttr = self.node.ss_hybridGrp
                self.hybridGrp = hybridAttr.listConnections()[0]
            except AttributeError:
            #if there's no hyrbid attr, we need to make the group and attr
                self.node.addAttr("ss_hybridGrp", at="message")
                self.hybridGrp = pm.group(world=True, empty=True, 
                                          name = "%s_ssHybridDriven" % self.node.name())
                snap(self.node, self.hybridGrp)
                #get the child object's parent - this returns empty if it's world
                drivenParent= self.drivenGrp.getParent()
                #make the child and dynParent group siblings if it's parent isn't world
                if drivenParent:
                    pm.parent(self.hybridGrp, drivenParent)
                self.hybridGrp.message >> self.node.ss_hybridGrp
                #add the enum
                self.node.addAttr("orientTo", at="enum", en="None", keyable = True)
                pm.pointConstraint(self.hybridGrp, self.drivenGrp, 
                                   name = "%s_ssHybridPoint_cst" % self.node.name())

    def removeGroups(self):
        #check if there are still any parents.  If there are't, there shouldn't
        #be constraints left
        if self.getParents():
            raise SSError ("Must first remove all parents using removeParent()")

        #make self.node a sibling of self.drivenGrp
        parent = self.drivenGrp.getParent()
        self.node.setParent(parent)
        pm.delete(self.drivenGrp)
        self.drivenGrp = None

        if self.node.ss_type.get() == "hybrid":
            #deleting the last point constraint to the hybrid group when using
            #removeParent seemed to sometimes delete the group itself
            try:
                pm.delete(self.hybridGrp)
            except pm.MayaNodeError:
                #just pass, as the node was already deleted
                pass
            self.hybridGrp = None
            for attr in ['orientTo', 'ss_hybridGrp']:
                if self.node.hasAttr(attr):
                    getattr(self.node, attr).delete()
        #remove the attrs
        for attr in ['parentTo', 'ss_drivenGrp','ss_type', CHILD_ATTR]:
            if self.node.hasAttr(attr):
                getattr(self.node, attr).delete()

    def addParent(self, parent, alias = None):
        """Add parent as a 'parent' object of child.  'Parent' type can be
	parent, or orient"""
        curParents = [node.name() for node in self.getParents().values()]

        if parent.node.name() in curParents:
            raise SSError("%s is already a parent of %s" % (parent.node.name(),
                                                            self.node.name()))

        #check parent Type
        if not isinstance(parent, Parent):
            raise SSBadInputError("parent must be a Parent object")
        ssType = self.node.ss_type.get()
        #add parent attribute
        newNum = getNextNum(self.node, "ss_parent")
        self.node.addAttr("ss_parent_%i" % newNum, at="message")
        prntAttr = pm.PyNode("%s.ss_parent_%i" % (self.node.name(), newNum))
        parent.node.message >> prntAttr
        #add an attribute for the alias of the attribute
        self.node.addAttr("ss_alias_%i" % newNum, dt="string")
        aliasAttr = pm.PyNode("%s.ss_alias_%i" % (self.node.name(), newNum))
        if alias:
            aliasAttr.set(alias)
        else:
            aliasAttr.set(parent.node.name())

        #create the constraint
        cstHyb = None
        cst = None
        if ssType == "parent":
            cst = pm.parentConstraint(parent.driverGrp, self.drivenGrp, mo=True,
                                      name = "%s_ssParent_cst" % self.node.name())
        elif ssType == "orient":
            cst = pm.parentConstraint(parent.driverGrp, self.drivenGrp, mo=True,
                                      name = "%s_ssParentNoTran_cst" % self.node.name(), st=["x", "y", "z"])
        elif ssType == "hybrid":
            #this constraint is to the hybrid group
            cst = pm.parentConstraint(parent.driverGrp, self.hybridGrp, mo=True,
                                      name = "%s_ssParent_cst" % self.node.name())
            cstHyb = pm.parentConstraint(parent.driverGrp, self.drivenGrp, mo=True,
                                         name = "%s_ssParentNoTran_cst" % self.node.name(), st=["x", "y", "z"])

        #add constraint attribute
        self.node.addAttr("ss_cst_%i" % newNum, at="message")
        cstAttr = pm.PyNode("%s.ss_cst_%i" % (self.node.name(), newNum))
        cst.message >> cstAttr

        #update the enum attr
        self.updateEnum()

        #create condition node and attach via messages
        cnd = pm.createNode("condition", name = "%s_ssParent_%i_cnd"
                            %(self.node.name(), newNum))
        self.node.addAttr("ss_cnd_%i" % newNum, at="message")
        cndAttr = pm.PyNode("%s.ss_cnd_%i" % (self.node.name(), newNum))
        cnd.message >> cndAttr

        #set up the condition node
        cnd.colorIfTrue.set([1,1,1])
        cnd.colorIfFalse.set([0,0,0])
        cnd.operation.set(0) #equal to
        self.node.parentTo >> cnd.firstTerm
        cnd.secondTerm.set(newNum)

        #get the attribute of the weight value for this parent and connect the 
        #condition to the weight

        driverGrp = parent.driverGrp
        cstWt = getCstWeightAttr(cst, driverGrp)
        cnd.outColorR >> cstWt

        #if the type is a hybrid, we have to set up additional conditions and connections
        if ssType == "hybrid":
            #add hybrid constraint attribute
            self.node.addAttr("ss_cstHyb_%i" % newNum, at="message")
            cstHybAttr = pm.PyNode("%s.ss_cstHyb_%i" % (self.node.name(), newNum))
            cstHyb.message >> cstHybAttr
            #create hybrid condition node and attach via messages
            cndHyb = pm.createNode("condition", name = "%s_ssParentHyb_%i_cnd"
                                   %(self.node.name(), newNum))
            self.node.addAttr("ss_cndHyb_%i" % newNum, at="message")
            cndHybAttr = pm.PyNode("%s.ss_cndHyb_%i" % (self.node.name(), newNum))
            cndHyb.message >> cndHybAttr
            #set up the condition node
            cndHyb.colorIfTrue.set([1,1,1])
            cndHyb.colorIfFalse.set([0,0,0])
            cndHyb.operation.set(0) #equal to
            self.node.orientTo >> cndHyb.firstTerm
            cndHyb.secondTerm.set(newNum)	
            #get the attribute of the weight value for this parent and connect the 
            #condition to the weight
            cstHybWt = getCstWeightAttr(cstHyb, driverGrp)
            cndHyb.outColorR >> cstHybWt

        #update the enum attrs
        self.updateEnum()
        
    def getAlias(self, parent):
        num = getParentNum(self, parent)
        aliasAttr = pm.PyNode("%s.ss_alias_%i" % (self.node.name(), num))
        return aliasAttr.get()

    def setAlias(self, parent, newAlias):
        num = getParentNum(self, parent)
        aliasAttr = pm.PyNode("%s.ss_alias_%i" % (self.node.name(), num))
        aliasAttr.set(newAlias)
        self.updateEnum()

    def removeParent(self, parent):
        """Remove the constraints and connections to the parent.
        If the parent is the last one to be removed, remove the groups
        on it as well."""
        ssType = self.node.ss_type.get()
        num = getParentNum(self, parent)
        cstAttr = pm.PyNode("%s.ss_cst_%i" % (self.node.name(), num))
        cndAttr = pm.PyNode("%s.ss_cnd_%i" % (self.node.name(), num))
        parentAttr = pm.PyNode("%s.ss_parent_%i" % (self.node.name(), num))
        aliasAttr = pm.PyNode("%s.ss_alias_%i" % (self.node.name(), num))
        #remove all commmon attrs, nodes, and connections
        cndNode = cndAttr.listConnections()[0]
        pm.delete(cndNode)
        cstAttr.delete()
        cndAttr.delete()
        parentAttr.delete()
        aliasAttr.delete()

        ssType = self.node.ss_type.get()
        if ssType != "hybrid":
            pm.parentConstraint(parent.driverGrp, self.drivenGrp, e=1, rm=1)
        elif ssType == "hybrid":
            #remove the hybrid stuff
            cstHybAttr = pm.PyNode("%s.ss_cstHyb_%i" % (self.node.name(), num))
            cndHybAttr = pm.PyNode("%s.ss_cndHyb_%i" % (self.node.name(), num))
            cndHyb = cndHybAttr.listConnections()[0]
            pm.delete(cndHyb)
            cstHybAttr.delete()
            cndHybAttr.delete()
            #remove the parent constraint on the group
            #if it's the last parent, delete the point cst first to avoid an 
            #object jumping
            if len(self.getParents()) == 1:
                pm.pointConstraint(self.hybridGrp, self.drivenGrp, e=1, rm=1)
            pm.parentConstraint(parent.driverGrp, self.hybridGrp, e=1, rm=1)
            pm.parentConstraint(parent.driverGrp, self.drivenGrp, e=1, rm=1)
            #sometimes deleting the parent constraint deletes the actual group,
            #so put it back
            hybGrpAttr = pm.PyNode("%s.ss_hybridGrp" % self.node.name())
            if not hybGrpAttr.listConnections():
                self.hybridGrp = pm.group(world=True, empty=True, 
                                          name = "%s_ssHybridDriven" % self.node.name())
                snap(self.node, self.hybridGrp)
                self.hybridGrp.message >> hybGrpAttr

        self.updateEnum()

    def updateEnum(self):
        """Update the enum attr on the child to list all parents."""
        #build the enum string
        enumStr = ""
        #numDict is "{i: 'nodeName'}" of parent nodes
        numDict = getNumDict(self.node, "ss_parent")
        nums = numDict.keys()
        nums.sort()
        #if there are any "ss_parent" attrs
        if nums:
            for i, num in enumerate(nums):
                aliasAttr = pm.PyNode("%s.ss_alias_%i" % (self.node.name(), num))
                alias = aliasAttr.get()
                if i == 0:
                    enumStr += ("%s=%i" % (alias, num))
                else:
                    enumStr +=(":%s=%i" % (alias, num))
        else:
            enumStr = "NONE:0"

        ##print "enum str is " + enumStr
        #update the enum
        pm.addAttr(self.node.parentTo, e=1, enumName = enumStr)
        #if hybrid, update that one too
        if self.node.ss_type.get() == "hybrid":
            pm.addAttr(self.node.orientTo, e=1, enumName = enumStr)

    def getParents(self):
        """Return a dictionary of 'parentNodeName': parentNode"""
        result = {}
        attrs = [attr for attr in self.node.listAttr(ud=True) if "ss_parent" 
                 in attr.name()]
        for attr in attrs:
            parentObj = attr.listConnections()[0]
            result[parentObj.name()] = parentObj
        return result

    def getParentNums(self):
        """Return a dictionary of {number: 'parentNode'} for all parents"""
        return getNumDict(self.node, "ss_parent")

    def getType(self):
        """Return a string of the type of child"""
        return self.node.ss_type.get()

###########################################################################
##---Helper Functions
###########################################################################

def snap(master, slave, point=True, orient=True):
    """snap the slave to the position and orientation of the master"""
    if point:
        pm.delete (pm.pointConstraint(master, slave, offset=[0,0,0], weight=1))
    if orient:
        pm.delete (pm.orientConstraint(master, slave, offset=[0,0,0], weight=1))

def getNextNum(node, attrPrefix):
    """Get the next numer in a series of sequential attrs on node.  Attrs are in 
    the style 'attrPrefix_#'"""
    node = pm.PyNode(node)
    #get a list of all attributes with attrPrefix in the name
    attrs = [attr for attr in node.listAttr(ud=True) if attrPrefix 
             in attr.name()]
    allNums = [int(attr.split("_")[-1]) for attr in attrs]
    #if there's nothing in allNums, we can return 0 immediately
    if not allNums:
        return 0

    comparisonRange = set(xrange(0, max(allNums) + 1))
    diff = comparisonRange.difference(allNums)
    #if there's no difference, then nums are in sequence so return the next
    if not diff:
        return max(allNums) + 1
    #else, return one of the missing numbers
    return diff.pop()

def getNumDict(node, attrPrefix):
    """Get a dictionary of number:'nodeName' for all sequential attrs on a node
    that start with attrPrefix.  'nodeName' is a string of the name of the
    connected node."""
    node = pm.PyNode(node)
    result = {}
    #get a list of all attributes with attrPrefix in the name
    attrs = [attr for attr in node.listAttr(ud=True) if attrPrefix 
             in attr.name()]
    for attr in attrs:
        result[int(attr.name().split("_")[-1])] = attr.listConnections()[0].name()
    return result

def getParentNum(child, parent):
    """get the number of a parent in the sequnce of parents connected to a child"""
    d = getNumDict(child.node, "ss_parent")
    for num, item in d.items():
        if parent.node == item:
            return num
    raise SSError("Could not find %s as a parent of %s" % (parent.node.name(), child.node.name()))

def getCstWeightAttr(constraint, master):
    """These attrs are sometimes tough to pin down as their number is based on assignment order.  
    This finds the correct attr and returns a PyNode attr."""

    cst = pm.PyNode(constraint)
    master = pm.PyNode(master)
    attrs = cst.listAttr(ud=1)
    for attr in attrs:
        try:
            targetWtAttr = attr.listConnections(p=1, c=1)[0][1]
            driverNode = targetWtAttr.getParent().targetParentMatrix.listConnections()[0]
            if driverNode == master:
                return attr
        except:
            continue
        
    # try:
    #     #this is the normal case
    #     attrStr = [attr for attr in pm.listAttr(constraint, userDefined=True) if master.name() in attr][0]
    # except IndexError:
    #     #this might be needed for referenced objects
    #     unrefMaster = master.name().split(":")[-1]
    #     attrStr = [attr for attr in pm.listAttr(constraint, userDefined=True) if unrefMaster in attr][0]
    # attr= pm.PyNode("%s.%s" % (constraint.name(), attrStr))
    # return attr


###########################################################################
##---creation functions
###########################################################################

def listChildren():
    """Return a list of all "child" objects in the scene"""
    childNodes = pm.ls("*.%s" % CHILD_ATTR, o=1, r=1)
    return [item.name() for item in childNodes]

def listParents():
    """Return a list of all "parent" objects in the scene"""
    result = set([])
    childObjs = [Child(childNode) for childNode in listChildren()]
    for child in childObjs:
        result.update(child.getParents().keys())
    return list(result)

def isChild(childStr):
    """return True if the child is a Child object, else return False"""
    allChildren = listChildren()
    if childStr in allChildren:
        return True
    return False

def isParent(parentStr):
    """return True if the parent is a Parent object, else return False"""
    parents = listParents()
    if parentStr in parents:
        return True
    return False

def addParent(childObj, parentStr, parentAlias = None):
    """Add parent to child."""
    if parentStr == childObj.node.name():
        raise SSBadInputError("Can't make an object a parent of itself!")
    p = Parent(parentStr)
    childObj.addParent(p, parentAlias)
    return p

def addChild(childStr, ssType):
    """Create a child object"""
    #check if it's a referenced object.  If so, it can't be a child
    
    if pm.referenceQuery(childStr, isNodeReferenced=1):
        pm.warning("\nCannot create children from %s because it is a referenced object.  Continuing..." % childStr)
        return -1
    return Child(childStr, ssType)

def removeParent (childStr, parentStr):
    """Remove a parent from child."""
    c= Child(childStr)
    p = Parent(parentStr)
    c.removeParent(p)
    print ("Removed %s as a parent of %s" %(p.node.name(), c.node.name()))

def removeAllParents(childStr):
    """Remove all parents from a child and reset the child"""
    c= Child(childStr)
    parents = [Parent(p) for p in c.getParents().values()]
    for parent in parents:
        c.removeParent(parent)
    c.removeGroups()

def getParents(childStr):
    """Return a list of all parents of child"""
    c = Child(childStr)
    return c.getParents().keys()

def removeChild (childStr):
    """Delete all space-switching nodes and connections on the child"""
    if pm.referenceQuery(childStr, isNodeReferenced=1):
        pm.warning("Cannot remove a referenced child node")
        return
    
    c = Child(childStr)
    
    parents = [Parent(item) for item in c.getParents().values()]

    for p in parents:
        c.removeParent(p)
    c.removeGroups()
def getAlias (childStr, parentStr):
    """Get the enum name of the parent for the particular child"""
    c = Child(childStr)
    p = Parent(parentStr)
    return c.getAlias(p)

def setAlias (childStr, parentStr, newAlias):
    """Get the enum name of the parent for the particular child"""
    c = Child(childStr)
    p = Parent(parentStr)
    c.setAlias(p, newAlias)

def getType(childStr):
    """Get the type of child"""
    c = Child(childStr)
    return c.getType()

def getCurrentParent(childStr):
    """Get the current parent"""
    c = Child(childStr)
    nums = c.getParentNums()
    curNum = c.node.parentTo.get()
    for num, par in nums.items():
        if num == curNum:
            return par

def getCurrentOrient(childStr):
    """Get the current parent"""
    c = Child(childStr)
    nums = c.getParentNums()
    try:
        curNum = c.node.orientTo.get()
        for num, par in nums.items():
            if num == curNum:
                return par
    except AttributeError:
        return None
    
###########################################################################
##---animation functions
###########################################################################

def switch(childStr, toParent, parent=True, hybrid=True):
    """Seamlessly switch from parent1 to parent2"""
    c = Child(childStr)
    tp = Parent(toParent)
    if tp.node.name() not in c.getParentNums().values():
        raise SSError("%s is not a parent of %s" % (tp.node.name(), c.node.name()))
    wsp = pm.xform(c.node, q=1, matrix=1, ws=1)
    for k,v in c.getParentNums().items():
        if tp.node.name() == v:
            if parent:
                c.node.parentTo.set(k)
            if hybrid:
                if hasattr(c.node, "orientTo"):
                    c.node.orientTo.set(k)
    pm.xform(c.node, matrix=wsp, ws=1)

###########################################################################
##---Maya UI
###########################################################################

class SsUI(object):
    def __init__(self):	
        self.name = "ssWin"
        self.title = "JP Space Switching"

        if pm.window(self.name, exists = True):
            pm.deleteUI(self.name)
        if pm.windowPref(self.name, exists=True):
            pm.windowPref(self.name, remove=True)

        template = pm.uiTemplate('uiTemplate', force=True )
        template.define(pm.frameLayout, cll=True, la="center", labelIndent=5, borderStyle="etchedIn", 
                        marginHeight=10, marginWidth=10)
        template.define(pm.button, width=180, height=40, align='left' )
        template.define(pm.columnLayout, adjustableColumn=True, columnAttach=('both', 0))
        template.define(pm.radioButtonGrp, nrb=2, labelArray2 = ["Parent + Orient", "Parent"],
                        cl2=["left", "left"], sl=1, ct2 = ["both", "both"], cw2 =[100,60])
        #create window
        self.win = pm.window(self.name, title = self.title + " -v" + __version__, w=470,h=510)
        pm.setUITemplate(template.name(), pushTemplate=True )
        tabs = pm.tabLayout(sc=self._updateAll)


        #CREATION layout#
        creation = pm.formLayout("Creation", numberOfDivisions=100)
        rtTopCl = pm.columnLayout()
        if True:
            pm.text("Type:", al="left", rs=True)
            self.typeFld = pm.textField(h=30, editable=False)
            pm.text(label="", h=10)
            pm.button(label = "- Remove Child", c=self.removeChild)
            pm.text(label="", h=10)
            pm.button(label = "+ Add Child", c=self.addChild)
            pm.text(label="", h=5)
            self.addChBtns = pm.radioButtonGrp()
            pm.text(label="", h=10)
            pm.button(label = "~ Change Type", c=self.changeChild)
            pm.text(label="", h=5)
            self.chChBtns = pm.radioButtonGrp()
            pm.setParent("..")
        lfTopCl = pm.columnLayout()
        if True:
            sscTxt = pm.text("All Space-Switching Children:", al="left")
            self.childList = pm.textScrollList(nr=15, sc = self._updateParents)
            pm.setParent("..")

        rtBtmCl = pm.columnLayout()
        if True:
            pm.text("Drop-down name of selected parent:", al="left", rs=True)
            self.nameFld = pm.textField(h=30)
            pm.text(label="", h=10)
            pm.button(label = "~ Change Name", c = self.changeName)
            pm.text(label="", h=10)
            addChBtn = pm.button(label = "+ Add Parent", c = self.addParent)
            pm.text(label="", h=10)
            chChBtn = pm.button(label = "- Remove Parent", c = self.removeParent)
            pm.setParent("..")
        lfBtmCl = pm.columnLayout()
        if True:
            self.prnTxt= pm.text("Parents of selected child:", al="left")
            self.parentList = pm.textScrollList(nr=14, sc = self._updateName)
            pm.setParent("..")

        #distribution#
        #top right Column
        creation.attachForm(rtTopCl, "top", 5)
        creation.attachForm(rtTopCl, "right", 5)
        creation.attachPosition(rtTopCl, "bottom", 5, 50)
        creation.attachNone(rtTopCl, "left")

        #top left Column
        creation.attachForm(lfTopCl, "top", 5)
        creation.attachControl(lfTopCl, "right", 5, rtTopCl)
        creation.attachForm(lfTopCl, "left", 5)
        creation.attachPosition(lfTopCl, "bottom", 5, 50)

        #btm right column
        creation.attachPosition (rtBtmCl, "top", 5, 50)
        creation.attachForm(rtBtmCl, "right", 5)

        creation.attachPosition(lfBtmCl, "top", 5, 50)
        creation.attachForm(lfBtmCl, "left", 5)
        creation.attachControl(lfBtmCl, "right", 5, rtBtmCl)
        creation.attachForm(lfBtmCl, "bottom", 5)
        pm.setParent(tabs)

        #ANIMATION Layout
        animation = pm.formLayout("Animation", numberOfDivisions=100)

        lfTopCl2 = pm.columnLayout()
        if True:
            sscTxt2 = pm.text("All Space-Switching Children:", al="left")
            self.childList2 = pm.textScrollList(nr=14, sc = self._updateParents2)
            pm.setParent("..")
        rtTopCl2 = pm.columnLayout()
        if True:
            pm.text("Current Parent:", al="left", rs=True)
            self.curParFld = pm.textField(h=30, editable=False)
            pm.text("Current Orient:", al="left", rs=True)
            self.curOrtFld = pm.textField(h=30, editable=False)
            
            self.prnTxt2= pm.text("Parents of selected child:", al="left")
            self.parentList2 = pm.textScrollList(nr=7)
            pm.setParent("..")
        btmCol2 = pm.columnLayout()
        if True:
            self.switchOpts = pm.checkBoxGrp(label = "Switch Options:", ncb=2, 
                                             la2=["Switch Parent", "Switch Orient"], 
                                             va2=[1, 1], cw3=[110,120,120])
            hl = pm.horizontalLayout()
            if True:
                switchBtn = pm.button(label = "Seamlessly Switch", c = self.seamlessSwitch)
                switchKeyBtn = pm.button(label = "Seamless Switch and Key", c = self.switchAndKey)
            hl.redistribute()
        #distribution#
        #top right Column
        animation.attachForm(rtTopCl2, "top", 5)
        animation.attachForm(rtTopCl2, "right", 5)
        animation.attachPosition(rtTopCl2, "bottom", 5, 50)
        animation.attachPosition(rtTopCl2, "left", 5, 50)

        #top left Column
        animation.attachForm(lfTopCl2, "top", 5)
        animation.attachControl(lfTopCl2, "right", 5, rtTopCl2)
        animation.attachForm(lfTopCl2, "left", 5)
        animation.attachPosition(lfTopCl2, "bottom", 5, 50)

        animation.attachControl(btmCol2, "top", 5, lfTopCl2)
        animation.attachForm(btmCol2, "right", 5)
        animation.attachForm(btmCol2, "left", 5)

        #SHOW WINDOW  
        self.win.show()
        self._updateChildren()
        self._updateChildren2()


    def _updateAll(self, *args):
        """Update everything in the UI"""
        self._updateChildren()
        self._updateChildren2()
    def _updateChildren(self, *args):
        """Populate/refresh the list of children"""
        current = self.childList.removeAll()
        children = listChildren()
        for child in children:
            self.childList.append(child)
        self._updateParents()

    def _updateChildren2(self, *args):
        """Populate/refresh the list of children"""
        current = self.childList2.removeAll()
        children = listChildren()
        for child in children:
            self.childList2.append(child)
        self._updateParents2()

    def _updateParents(self, *args):
        """Update the type and parents list"""
        #update type box
        children = self.childList.getSelectItem()
        if not children:
            self.typeFld.setText("")
            self.parentList.removeAll()
            return
        child = children[0]
        sstype = getType(child)
        if sstype == "hybrid":
            sstype = "parent + orient"
        self.typeFld.setText(sstype)
        #update the parents list
        self.parentList.removeAll()
        parents = getParents(child)
        for parent in parents:
            self.parentList.append(parent)
        self._updateName()
        self._updateParents2()

    def _updateParents2(self, *args):
        """Update the current parent field and parents list"""
        #update type box
        children = self.childList2.getSelectItem()
        if not children:
            self.parentList2.removeAll()
            self.curParFld.setText("")
            return
        child = children[0]
        #update the parents list
        self.parentList2.removeAll()
        parents = getParents(child)
        for parent in parents:
            name = "%s - %s" % (getAlias(child, parent), parent)
            self.parentList2.append(name)
        parent = getCurrentParent(child)
        orient = getCurrentOrient(child)
        if not parent:
            self.curParFld.setText("")
            return
        self.curParFld.setText(getAlias(child, parent))
        if not orient:
            self.curOrtFld.setText("")
            return
        self.curOrtFld.setText(getAlias(child, orient))

    def _updateName(self, *args):
        """Update the parent name field"""
        children = self.childList.getSelectItem()
        parents = self.parentList.getSelectItem()
        if not children or not parents:
            self.nameFld.setText("")
        else:
            name = getAlias(children[0], parents[0])
            self.nameFld.setText(name)

    def _getRbLabel(self, control):
        """Returns the selected radio button label"""
        num = control.getSelect()
        if num == 1:
            return "hybrid"
        elif num == 2:
            return "parent"
        elif num == 3:
            return "orient"

    def addChild(self, *args):
        """Add the currently selected object as a child"""
        children = [child for child in pm.ls(sl = True) if isinstance (child, pm.nt.Transform)]
        ssType = self._getRbLabel(self.addChBtns)
        errorList = []
        for child in children:            
            errorList.append(addChild(child.name(), ssType))
        self._updateChildren()
        self._updateParents()
        #check if there's a -1 in the list...if so, something didn't get created
        if -1 in errorList:
            pm.promptDialog(m="Some children were not added.  See Script Editor for details.")

    def removeChild(self, *args):
        """Remove currently selected object as a child"""
        children = self.childList.getSelectItem()
        if not children:
            children = [child.name() for child in pm.ls(sl=True) if isinstance(child, pm.nt.Transform)]
        for child in children:
            removeChild(child)
        self._updateChildren()
        self._updateParents()

    def changeChild(self, *args):
        """Add the currently selected object as a child"""
        children = self.childList.getSelectItem() or []
        newType = self._getRbLabel(self.chChBtns)
        for child in children:
            parents = getParents(child)
            removeChild(child)
            c = addChild(child, newType)
            for parent in parents:
                addParent(c, parent)            
        self._updateChildren()
        self._updateParents()

    def addParent(self, *args):
        """Add the currently selected objects as parents"""
        #get the child
        children = self.childList.getSelectItem()
        if not children:
            raise SSInterfaceError("Select a child to add a parent to")
        child = Child(children[0])
        #get the parents:
        parents = [parent.name() for parent in pm.ls(sl = True) if isinstance(parent, pm.nt.Transform)]
        for parent in parents:
            addParent(child, parent)
        self._updateParents()

    def removeParent(self, *args):
        """Remove the selected parents from the child"""
        print ("removing parent")
        #get the child
        children = self.childList.getSelectItem()
        if not children:
            raise SSInterfaceError("Select a child to remove a parent from")
        child = children[0]
        #get the parents:
        parents = self.parentList.getSelectItem()
        if not parents:
            raise SSInterfaceError("Select a parent to remove")
        for parent in parents:
            removeParent(child, parent)
        self._updateParents()

    def changeName(self, *args):
        """Change the parent alias to the text in the parentName field"""
        children = self.childList.getSelectItem()
        parents = self.parentList.getSelectItem()
        if not children or not parents:
            raise SSInterfaceError("must select a child and parent to rename")
        else:
            newName = self.nameFld.getText()
            print "new name is " + newName
            setAlias(children[0], parents[0], newName)

    def seamlessSwitch(self, *args):
        """Seamlessly switch from one parent to another."""
        child = self.childList2.getSelectItem()[0]
        #parents are formatted "parentAlias : parent"
        parent = self.parentList2.getSelectItem()[0].split("-")[-1].strip()
        cmd = "switch('%s', '%s'" % (child, parent)
        if not self.switchOpts.getValue1():
            cmd +=", parent=False"
        if not self.switchOpts.getValue2():
            cmd +=", hybrid=False"
        cmd += ")"
        eval(cmd)
        self._updateChildren2()
        #reselect the previously selected child, and refresh the Parents
        self.childList2.setSelectItem(child)
        self._updateParents2()
        
        return [child, parent]

    def switchAndKey(self, *args):
        """Seamlessly switch from one parent to another, and set the proper
	keyframes"""
        attrs = ["tx", "ty", "tz", "rx", "ry", "rz"]
        if self.switchOpts.getValue1():
            attrs.append("parentTo")
        if self.switchOpts.getValue2():
            attrs.append("orientTo")
        child = pm.PyNode(self.childList2.getSelectItem()[0])
        time = pm.currentTime()
        #if there is not "orientTo", it just passes
        pm.setKeyframe(child, attribute = attrs, t=[time-1])
        self.seamlessSwitch()
        pm.setKeyframe(child, attribute = attrs, t=[time])
        self._updateChildren2()
        #reselect the previously selected child, and refresh the Parents
        self.childList2.setSelectItem(child)
        self._updateParents2()

def initUI():
    """Fire up the maya UI"""
    return SsUI()
