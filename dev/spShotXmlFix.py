import SpImport
import Ti.Factory
import time, datetime
f = Ti.Factory(showname='cl2')

VP = SpImport.Package('Vnp3', 2)
SX = SpImport.Package('ShotXML',3)

s = VP.getShow('cl2')
f = s.getRootFolder()
shots = [x for x in f.getChildFolderNames() if re.match('^cvf[0-9]+.*', x)]

toOmit = []
for shot in shots:
    animTask = f.taskSearch(shotname=shot, taskname=['Animation'])[0]
    status = animTask.str_x_asset_task_status
        toOmit.append(shot)

for shot in toOmit:
    shot_db = SX.ShotXML(show='cl2', shot=shot, lock=False)
    try:
        char_master= shot_db.getAssetsByName('char_master')[0]
    except IndexError:
        continue
    pt_kids = char_master.getChildrenByName("policetape_all")
    if pt_kids:
        e = pt_kids[0]
        if '.mod_rlo' in e.getSpref():
            print "DELETING rlo policetape from %s" % shot
            shot_db.lock()
            e.setAssetStatus(SX.SX_AssetStatus.DELETED)
            shot_db.publishGroupTree()
            shot_db.versionUp()
            shot_db.unlock()
        else:
            print "SKIPPING deletion from %s" % shot
            
            
