"""
Description:
    This module assists with orienting a joint so that it's twist axis points along a vector
    perpendicular to a plane formed by itself, its parent, and its child.

    This orients the joint so that it will roll back exactly in line with it's parent.  When
    creating joints along a viewport plane, this behavior is automatic, but when positioning non-planar joints,
    it's tougher to achieve

Usage:
    1) ensure the script is placed in one of the Maya script paths
    2) import the module by typing "import orientToPlane" on the python commandline.
    3) activate the UI with "orientToPlane.initUI()
    
Dependencies:
    Pymel must be installed.  Get it from http://code.google.com/p/pymel/

"""

__author__ = "Author: John Patrick (jspatrick@gmail.com)"
__version__ = "Revision: 0.1"
__date__ = "Date: 9/15/2009"


import pymel.core as pm


def addSel(control):
    sel = pm.ls(sl=True)[0]
    control.setText(sel)
    return sel

def addAndPredict(joFld, jaFld, jbFld):
    "Use this for midPos callback"
    sel = pm.PyNode(addSel(joFld))
    parent = sel.getParent()
    child = sel.getChildren()[0]
    jaFld.setText(parent)
    jbFld.setText(child)
    
def getXProduct(objList):
    """
    get a cross product based upon the position of the first three objects in objList
    Returns the cross product of two vectors: objList[0] to objList[1] and objList[0] to objList[2]
    """ 

    #get the proper joints
    midJnt = objList[0]
    topJnt = objList[1]
    btmJnt = objList[2]

    #get the vectors
    midPos = pm.xform(midJnt, q=True, worldSpace=True, translation=True)
    topPos = pm.xform(topJnt, q=True, worldSpace=True, translation=True)
    btmPos = pm.xform(btmJnt, q=True, worldSpace=True, translation=True)

    topVector = [topPos[0]- midPos[0], topPos[1]- midPos[1], topPos[2]- midPos[2]]
    btmVector = [btmPos[0]- midPos[0], btmPos[1]- midPos[1], btmPos[2]- midPos[2]]

    #create a temporary vectorProduct node
    vp = pm.createNode("vectorProduct")
    #set to cross product
    vp.operation.set(2)
    vp.input1.set(btmVector)
    vp.input2.set(topVector)

    #store the cross product
    cp = vp.output.get()

    #delete the vector node
    pm.delete(vp)
    return cp

def orientJoint(joint, worldUpVec, aimVector=[1,0,0], upVector=[0,0,1]):
    #if either joint or loc not provided, query from selection
    joint = pm.PyNode(joint)
    try:
        childJnt = [x for x in joint.listRelatives(children=True) if isinstance(x, pm.nt.Joint)][0]
    except IndexError:
        pm.joint(joint, e=1, oj="none")
        return
    pm.parent(childJnt, world=True)

    #zero orients
    joint.jointOrientX.set(0)
    joint.jointOrientY.set(0)
    joint.jointOrientZ.set(0)

    #aim at next jnt using up loc
    aimCst = pm.aimConstraint (childJnt, joint, offset=[0,0,0], aimVector= aimVector, upVector=upVector, worldUpType = "vector",
                               worldUpVector = worldUpVec)

    pm.delete(aimCst)
    #get rotations
    rots = joint.rotate.get()
    #zero rotations
    joint.rotate.set([0,0,0])
    #set orients
    joint.jointOrientX.set(rots[0])
    joint.jointOrientY.set(rots[1])
    joint.jointOrientZ.set(rots[2])
    #reparent child joint
    pm.parent (childJnt, joint)

def orientToPlane(objList, aimVecGrp, upVecGrp, flipAim, flipUp):
    #get the aim vector
    aimVector = [0,0,0]
    #getSelect() returns an integer that indicates the currently selected button in the radioGroup, and is 1-based
    for num in range(1,4):
        if aimVecGrp == num:
            #check the value of the "flip aim" control in the UI
            if flipAim:
            #the getSelect() is 1-based, but lists are 0-based, so subtract 1
                aimVector[num-1] = -1
            else:
                aimVector[num-1] = 1

    #get the upVector
    upVector = [0,0,0]
    for num in range(1,4):
        if upVecGrp == num:
            if flipUp:
                upVector[num-1] = -1
            else:
                upVector[num-1] = 1

    worldUpVec = getXProduct(objList)
    orientJoint(objList[0], worldUpVec, aimVector, upVector)
    pm.select(objList[0])
    
def initUI():
    """
    Fire up the UI
    
    """
    #----Window Check----#
    if pm.window("mainWindow", exists=True):
        pm.deleteUI("mainWindow")
    if pm.windowPref("mainWindow", exists=True):
        pm.windowPref("mainWindow", remove=True)

    #----Window Creation----#
    win = pm.window("mainWindow", title= "Orient to Plane")

    #----Window Layout----#
    if pm.window("mainWindow", exists=True):
        pm.deleteUI("mainWindow")
    if pm.windowPref("mainWindow", exists=True):
        pm.windowPref("mainWindow", remove=True)

    #----Window Creation----#
    win = pm.window("mainWindow", title= "Orient to Plane")

    #----Window Layout----#
    base = pm.verticalLayout(w=350, h=100, ratios = [0,0,0,0,0,0,0], spacing = 5)

    joFld = pm.textFieldButtonGrp(label="Joint To Orient", cw3=[100, 200, 50], buttonLabel="<<<", ad3=2, bc=lambda *args: addAndPredict(joFld, jaFld, jbFld))
    jaFld = pm.textFieldButtonGrp(label="Joint Above", cw3=[100, 200, 50], buttonLabel="<<<", ad3=2, bc=lambda *args: addSel(jaFld))
    jbFld = pm.textFieldButtonGrp(label="Joint Below", cw3=[100, 200, 50], buttonLabel="<<<", ad3=2, bc=lambda *args: addSel(jbFld))

    aimVecGrp = pm.radioButtonGrp(label = "Aim Axis", labelArray3 = ['x', 'y', 'z'], cw4=[100, 75, 75, 75], ct4= ["right", "both", "both", "both"],  nrb=3)
    upVecGrp = pm.radioButtonGrp(label = "Up Axis", labelArray3 = ['x', 'y', 'z'], cw4=[100, 75, 75, 75], ct4= ["right", "both", "both", "both"],  nrb=3)

    flipLayout = pm.horizontalLayout()
    flipAim = pm.checkBox(label = "Flip Aim Axis")
    flipLayout.attachForm(flipAim, "left", 100)

    flipUp = pm.checkBox(label = "Flip Up Axis")
    flipLayout.attachControl(flipUp, "left", 5, flipAim)


    pm.setParent("..")

    goBttn= pm.button(label = "Do It!", c=lambda *args: orientToPlane([joFld.getText(), jaFld.getText(), jbFld.getText()],
                                                                        aimVecGrp.getSelect(),
                                                                        upVecGrp.getSelect(),
                                                                        flipAim.getValue(),
                                                                        flipUp.getValue()))

    base.redistribute()

    win.show()
