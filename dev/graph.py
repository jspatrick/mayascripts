"""
"""
import weakref

class Vertex(object):
    def __init__(self, val):
        self.val = val
        self.edges = []
        
    def __eq__(self, other):
        if self.val == other.val:
            return True
        return False

    def __ne__(self, other):
        if self.val == other.val:
            return False
        return True
        
    def __hash__(self):
        return hash(str(self.val))

    def __str__(self):
        return str(self.val)

    def __repr__(self):
        return 'Vertex(%r)' % self.val

    def addEdge(self, e):
        assert e not in self.edges, "%r already exists" % e
        self.edges.append(e)

    def removeEdge(self, e):
        if e in self.edges:
            i = self.edges.index(e)
            e = self.edges.pop(i)

    def connectedVertices(self, byValue=False):
        result = []
        for edge in self.edges:
            if edge.head == self:
                if byValue:
                    v = edge.tail.val
                else:
                    v = edge.tail
                result.append(v)
            else:
                if byValue:
                    v = edge.head.val
                else:
                    v = edge.head
                result.append(v)
                
        return result

        
class Edge(object):
    def __init__(self, head, tail):
        assert head != tail, "Cannot form self-loop"
        
        self.__head = weakref.ref(head)
        self.__tail = weakref.ref(tail)
        
    @property
    def head(self):
        return self.__head()

    @property
    def tail(self):
        return self.__tail()

        
    def __eq__(self, other):
        if other.head.val == self.head.val and other.tail.val == self.tail.val:
            return True
        return False

    def __ne__(self, other):
        if self == other:
            return False
        return True
        
    def __hash__(self):
        return hash('%s_%s' % (str(self.head.val), str(self.tail.val)))

    def __repr__(self):
        return "Edge(%r, %r)" % (self.head, self.tail)

        
class Graph(object):
    def __init__(self):
        self._vertices = []
        self._edges = []
        
    def addVertex(self, val):
        v = Vertex(val)
        if v not in self._vertices:
            self._vertices.append(v)
        
    def addEdge(self, val1, val2):
        v1 = Vertex(val1)
        v2 = Vertex(val2)
        
        assert v1 in self._vertices
        assert v2 in self._vertices

        v1 = self._vertices[self._vertices.index(v1)]
        v2 = self._vertices[self._vertices.index(v2)]        
        
        e = Edge(v1, v2)
        if e not in self._edges:
            self._edges.append(e)
            
        v1.addEdge(e)
        v2.addEdge(e)
        
    def getEdges(self, val):
        i = self._vertices.index(Vertex(val))
        result = []
        for e in self._vertices[i].edges:
            result.append((e.head.val, e.tail.val))
        return result

    def _vertex(self, val):
        i = self._vertices.index(Vertex(val))
        return self._vertices[i]

    def _edge(self, val1, val2):
        v1 = self._vertex(val1)
        v2 = self._vertex(val2)        
        e = Edge(v1, v2)        
        return self._edges[self._edges.index(e)]
        
    def removeEdge(self, val1, val2):
        e = self._edge(val1, val2)
        v1 = self._vertex(val1)
        v2 = self._vertex(val2)
        v1.removeEdge(e)
        v2.removeEdge(e)        
        self._edges.pop(self._edges.index(e))

        
    def removeVertex(self, val):
        v = self._vertex(val)
        for edge in v.edges:
            headV = edge.head
            tailV = edge.tail
            if headV != v:
                headV.removeEdge(edge)
            else:
                tailV.removeEdge(edge)
                self._edges.pop(self._edges.index(edge))
                
        self._vertices.pop(self._vertices.index(v))

        
    def collapseEdge(self, val1, val2):
        """
        Merge two vertices
        """
        
