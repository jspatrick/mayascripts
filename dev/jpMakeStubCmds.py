#!/usr/autodesk/maya/bin/mayapy
from __future__ import with_statement
import sys, optparse, os, logging, re
logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('makeStubCmds')

try:
    import maya.cmds as MC
    import maya.mel as MM    
    import maya.app.commands

except ImportError:
    _logger.warning("Cannot import maya")
if MC.about(batch=1):
    import maya.standalone
    maya.standalone.initialize()

def getCmds():
    commandListPath = os.path.realpath( os.environ[ 'MAYA_LOCATION' ] )
    platform = maya.cmds.about( os=True )
    commandListPath = os.path.join(commandListPath,
                                   maya.app.commands.commandListLocations[platform],
                                   'commandList' )
    commands = []
    with open( commandListPath, 'r' ) as f:
        for line in f:
            commandName, library = line.split()
            commands.append(commandName)
    return commands

def getCmdString(cmd):

    helpStr = MM.eval('help %s' % cmd)
    helpStr = re.sub('\n', '\n    ', helpStr.strip())
    result = '''def %(cmd)s(*args, **kwargs):
    """
    %(helpStr)s
    """
    pass
    
''' % locals()
            
    return result

def _expand(p):
    return os.path.expandvars(os.path.expanduser(p))

def makeStubFile(filePath):
    if os.path.exists(filePath):
        os.remove(filePath)
    commands = getCmds()
    with open(_expand(filePath), 'w') as f:
        for cmd in commands:            
            f.write(getCmdString(cmd))
            
if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option("-d", "--dir", dest='dir',
                      help= "create files in direcory DIR")
    options, args = parser.parse_args()
    baseDir = _expand(options.dir)
    if not os.path.exists(baseDir):
        raise Exception("%s does not exist" % baseDir)
    f = os.path.join(baseDir, 'cmds.py')
    allCmds = getCmds()
