import os
import maya.cmds as MC
import SpImport
VP = SpImport.Package('Vnp3', 2)

def fixAsmbProps():
    couldNotFix = set()
    for ent in MC.ls(type='spEntityNode'):
        shot = os.environ['SHOT']
        show = os.environ['SHOW'] 

        if not MC.spEntityQuery(ent, type=1) == "procedural_prop_asmb":
            continue


        for mt in MC.spEntityQuery(ent, mts=1):

            #fix spref
            spref = MC.spEntityQuery(ent, mt=mt, asmbSpref=1)
            vpSpref = VP.Spref(spref)
            fix=False
            if vpSpref.getShow() != show:
                fix=True
            elif vpSpref.getShot() != shot:
                fix=True            
            if fix:
                newSpref = '{spref:cl2/sar115/env_master.asmb?v=current}'
                print "%s --> %s" % (spref, newSpref)
                MC.spEntityEdit(ent, mt=mt, asmbSpref=newSpref)

            #fix path
            path = MC.spEntityQuery(ent, mt=mt, asmbPath=1)
            if not MC.objExists(path):
                nodes = MC.ls(path.split('|')[-1], l=1)
                if len(nodes) != 1:
                    print "ERROR:  cannot find %s" % path
                    couldNotFix.add(ent)
                    continue

                newPath = 'env_master|%s' % nodes[0].split('env_master|')[-1]
                print '%s --> %s' % (path, newPath)
                MC.spEntityEdit(ent, mt=mt, asmbPath=newPath)
                
    return list(couldNotFix)
